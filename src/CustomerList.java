import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomerList implements IDisplay 
{

	private ArrayList<Customer> customers = new ArrayList<>();
	
	public void addCustomer(Customer customer) 
	{
		this.customers.add(customer);
	}

	public Customer getCustomerById(int id) 
	{
		Customer c = null;
		for (Customer customer : customers) 
		{
			if (customer.getCustomerId() == id) 
				c = customer;
			
		}

		if (c == null) 
		{
			System.out.println("No customer found with ID : " + id);

		}
		return c;
	}

	@Override
	public void display() 
	{
		Collections.sort(customers, new CustomerComparator());
		for (Customer customer : customers) 
		{
			customer.display();
		}
	}

}

class CustomerComparator implements Comparator<Customer> 
{

	@Override
	public int compare(Customer c1, Customer c2) 
	{
		if (c1.getCustomerId() < c2.getCustomerId()) 
		{
			return -1;
		} 
		else if (c1.getCustomerId() > c2.getCustomerId()) 
		{
			return 1;
		}
		else 
		{
			return 0;
		}
	}

}