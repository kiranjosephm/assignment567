import java.util.Date;
public class Bill implements IDisplay
{
		private int billId;
		private Date billDate;
		private String billType;
		private double totalbillAmount;
		
		public Bill(int billId, Date billDate, String billType, double totalbillAmount) 
		{
			this.billId = billId;
			this.billDate = billDate;
			this.billType = billType;
			this.totalbillAmount = totalbillAmount;
		}
		
		public int getBillId() 
		{
			return billId;
		}

		public void setBillId(int billId) 
		{
			this.billId = billId;
		}

		public Date getBillDate() 
		{
			return billDate;
		}

		public void setBillDate(Date billDate) 
		{
			this.billDate = billDate;
		}

		public String getBillType() 
		{
			return billType;
		}

		public void setBillType(String billType) 
		{
			this.billType = billType;
		}

		public double getTotalbillAmount() {
			return totalbillAmount;
		}

		public void setTotalbillAmount(double totalbillAmount) {
			this.totalbillAmount = totalbillAmount;
		}

		@Override
		public void display() 
		{
			System.out.println("Bill Id :" + billId);
			System.out.println("Bill Date :" + billDate);
			System.out.println("Bill Type :" + billType);
			System.out.println("Total Bill Amount : $" + totalbillAmount);
		}

}
