import java.util.ArrayList;
import java.util.Date;

public class Main 
{
	public static void main(String[] args) 
	{
		Customer kiran = new Customer(1, "kiran", "Jospeh", "KJ@gmail.com");
		Bill b1 = new Hydro(1, new Date(), "Hydro", 267.75, "TorontoHydro", 222);
		Bill b2 = new Internet(2, new Date(), "Internet", 51.75, "fido", 300);
		Bill b3 = new Mobile(3, new Date(), "Mobile", 699, "Oneplus 6t", "Postpaid",1234567890, 4.5, 200);
		ArrayList<Bill> bills = new ArrayList<>();
		bills.add(b2);
		bills.add(b1);
		bills.add(b3);
		kiran.setBills(bills);

		Customer jerin = new Customer(2, "jerin", "PK", "pkj@gmail.com");
		Bill b4 = new Hydro(1, new Date(), "Hydro", 100, "TorontoHydro", 99);
		Bill b5 = new Internet(2, new Date(), "Internet", 40, "Rogers", 200);
		ArrayList<Bill> bills1 = new ArrayList<>();
		bills1.add(b4);
		bills1.add(b5);
		jerin.setBills(bills1);

		Customer jayan = new Customer(3, "jayan", "Kalarikkal", "jayan@gmail.com");

		CustomerList cus = new CustomerList();
		cus.addCustomer(kiran);
		cus.addCustomer(jerin);
		cus.addCustomer(jayan);
		cus.display();

		System.out.println("\n_________Searching for a valid customer____________");
		Customer c1 = cus.getCustomerById(1);
		c1.display();

		
		System.out.println("\n_________Searching for an invalid customer__________");
		Customer c2=cus.getCustomerById(5);
	}
}
