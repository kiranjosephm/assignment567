import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Customer implements IDisplay 
{

	private int customerId;
	private String firstName;
	private String lastName;
	private String emailID;
	private ArrayList<Bill> bills;
	
	public Customer(int customerId, String firstName, String lastName, String emailID) 
	{
		this.customerId = customerId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailID = emailID;
	}

	public int getCustomerId() 
	{
		return customerId;
	}

	public void setCustomerId(int customerId) 
	{
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	public String getLastName() 
	{
		return lastName;
	}

	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	public String getEmailID() 
	{
		return emailID;
	}

	public void setEmailID(String emailID) 
	{
		this.emailID = emailID;
	}

	public ArrayList<Bill> getBills() 
	{
		return bills;
	}

	public void setBills(ArrayList<Bill> bills) 
	{
		this.bills = bills;
	}

	public String FullName() 
	{
		return firstName + " " + lastName;
	}

	public double getTotalBill() 
	{
		if (bills == null || bills.size() == 0)
			return 0;

		double total = 0;
		for (Bill bill : bills) 
		{
			total += bill.getTotalbillAmount();
		}
		return total;
	}

	@Override
	public void display() {
		System.out.println("Customer Id: " + customerId);
		System.out.println("Customer Full Name: " + this.FullName());
		System.out.println("Customer Email Id: " + emailID);
		if (this.getTotalBill() == 0) 
		{
			System.out.println(" No bills to display ");
			System.out.println("*************************************************");
			return;
		}
		System.out.println("             ____Bill Information____               ");
		System.out.println("****************************************************");
		Collections.sort(this.bills, new BillComparator());
		for (Bill bill : this.bills) 
		{
			bill.display();
		}
		System.out.println("    Total Amount to Pay: $" + this.getTotalBill());
		System.out.println("****************************************************");

	}

}

class BillComparator implements Comparator<Bill> 
{

	@Override
	public int compare(Bill b1, Bill b2) 
	{
		if (b1.getBillId() < b2.getBillId()) 
		{
			return -1;
		} 
		else if (b1.getBillId() > b2.getBillId()) 
		{
			return 1;
		} 
		else 
		{
			return 0;
		}
	}

}